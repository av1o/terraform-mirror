# Terraform Mirror

This application implements the [Provider Network Mirror Protocol](https://www.terraform.io/docs/internals/provider-network-mirror-protocol.html).

It currently only supports the S3 backend, however more can be added.

## Providers

1. Download the appropriate mirrors from `registry.terraform.io` or another registry.
1. Upload to S3 using the following directory structure
    ```
   {hostname}/{namespace}/{type}/{version}/terraform-provider-{type}_{version}_{os}_{arch}.zip
   registry.terraform.io/hashicorp/helm/1.3.2/terraform-provider-helm_1.3.2_linux_amd64.zip
   ```
   
## Configuration

| Name | Value | Description |
| --- | --- | --- |
| `port` | `8080` | Port to run on |
| `debug` | `false` | Enables debug logging |
| `cert` | | Path to TLS certificate |
| `key` | | Path to TLS key |
| `s3-bucket` | | Name of the S3 bucket |
| `s3-region` | `us-west-1` | Name of the S3 region |
| `s3-endpoint` | | Name of the S3 endpoint if not using AWS
| `s3-path-style` | `false` | Whether to use DNS (my-bucket.s3.amazonaws.com) or Path (s3.amazonaws.com/my-bucket) style buckets

## Usage

Create a `.terraformrc` file in your home directory
```
provider_installation {
 network_mirror {
   url = "https://localhost:8080/providers/"
 }
}
```