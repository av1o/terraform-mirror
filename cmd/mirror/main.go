package main

import (
	"fmt"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/gorilla/mux"
	"github.com/namsral/flag"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/tf-mirror/pkg/api"
	"gitlab.dcas.dev/open-source/tf-mirror/pkg/registry"
	"net/http"
)

func main() {
	objectConfig := &registry.S3Config{}

	port := flag.Int("port", 8080, "port to run on")
	debug := flag.Bool("debug", false, "enables debug logging")
	metrics := flag.Bool("metrics", true, "export Prometheus metrics on /metrics")

	cert := flag.String("cert", "", "path to tls certificate")
	key := flag.String("key", "", "path to tls key")

	flag.StringVar(&objectConfig.Bucket, "s3-bucket", "", "s3 bucket to use")
	flag.StringVar(&objectConfig.Region, "s3-region", "us-west-1", "s3 region to use")
	flag.StringVar(&objectConfig.Endpoint, "s3-endpoint", "", "s3 endpoint to use")
	flag.BoolVar(&objectConfig.PathStyle, "s3-path-style", false, "whether to use S3 DNS or path style buckets (e.g. mybucket.s3.amazonaws.com vs s3.amazonaws.com/mybucket)")

	flag.Parse()

	// configure logging
	if *debug {
		log.SetLevel(log.DebugLevel)
		log.Debug("enabled debug logging")
	} else {
		log.SetLevel(log.InfoLevel)
	}

	// setup services
	rego := registry.NewS3Registry(objectConfig)
	svc := api.NewMirrorService(rego)

	// setup router
	router := mux.NewRouter()

	router.HandleFunc("/providers/{hostname}/{namespace}/{type}/index.json", tracer.NewFunc(svc.AvailableVersions))
	router.HandleFunc("/providers/{hostname}/{namespace}/{type}/{version}.json", tracer.NewFunc(svc.PackageMeta))

	router.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"userAgent":  r.UserAgent(),
			"remoteAddr": r.RemoteAddr,
		}).Debug("answering probe")
		_, _ = w.Write([]byte("OK"))
	})
	if *metrics {
		log.Debug("enabling prometheus metrics")
		router.Handle("/metrics", promhttp.Handler())
	}

	// start server
	addr := fmt.Sprintf(":%d", *port)
	log.Infof("starting server on interface %s", addr)
	if *cert != "" && *key != "" {
		log.Info("starting server in secure mode")
		log.Fatal(http.ListenAndServeTLS(addr, *cert, *key, router))
	} else {
		log.Info("starting server in insecure mode")
		log.Fatal(http.ListenAndServe(addr, router))
	}
}
