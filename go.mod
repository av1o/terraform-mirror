module gitlab.dcas.dev/open-source/tf-mirror

go 1.15

require (
	github.com/aws/aws-sdk-go v1.35.31
	github.com/djcass44/go-tracer v0.2.0
	github.com/djcass44/go-utils v0.1.3-0.20201113102115-5142d9df90f1
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/go-version v1.2.0
	github.com/hashicorp/terraform v0.13.5
	github.com/hashicorp/terraform-svchost v0.0.0-20191011084731-65d371908596
	github.com/namsral/flag v1.7.4-pre
	github.com/prometheus/client_golang v1.9.0
	github.com/sirupsen/logrus v1.7.0
	gitlab.dcas.dev/open-source/tf-registry v0.0.0-20201123133430-4d5370aa744d
)
