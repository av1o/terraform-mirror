package api

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	MetricListSuccess = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_list_success_total",
		Help: "Total number of successful listings",
	})
	MetricListErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_list_errs_total",
		Help: "Total number of failed listings",
	})
	MetricGetSuccess = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_get_success_total",
		Help: "Total number of successful metadata retrievals",
	})
	MetricGetErrors = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_get_errs_total",
		Help: "Total number of failed metadata retrievals",
	})
	MetricNotFound = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_not_found_total",
		Help: "Total number of 404s returned",
	})
)
