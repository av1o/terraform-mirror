package api

import (
	"github.com/djcass44/go-tracer/tracer"
	"github.com/djcass44/go-utils/pkg/httputils"
	"github.com/gorilla/mux"
	"github.com/hashicorp/go-version"
	svchost "github.com/hashicorp/terraform-svchost"
	"github.com/hashicorp/terraform/addrs"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/tf-mirror/pkg/registry"
	"net/http"
)

type MirrorService struct {
	registry registry.ProviderRegistry
}

func NewMirrorService(registry registry.ProviderRegistry) *MirrorService {
	svc := new(MirrorService)
	svc.registry = registry

	return svc
}

// AvailableVersions determines which versions are currently available for a particular provider
// https://www.terraform.io/docs/internals/provider-network-mirror-protocol.html#list-available-versions
func (svc *MirrorService) AvailableVersions(w http.ResponseWriter, r *http.Request) {
	id := tracer.GetRequestId(r)
	vars := mux.Vars(r)
	hostname := vars["hostname"]
	namespace := vars["namespace"]
	name := vars["type"]

	provider := &addrs.Provider{
		Type:      name,
		Namespace: namespace,
		Hostname:  svchost.Hostname(hostname),
	}
	log.WithField("id", id).Infof("loaded provider: %s", provider.ForDisplay())

	versions, err := svc.registry.AvailableVersions(r.Context(), provider)
	if err != nil {
		log.WithError(err).WithField("id", id).Error("failed to list available versions")
		http.Error(w, "failed to list available versions", http.StatusBadRequest)
		MetricListErrors.Inc()
		return
	}
	if len(versions.Versions) == 0 {
		log.WithField("id", id).Info("found 0 versions, lets assume that means this provider doesn't exist")
		http.Error(w, "404 not found", http.StatusNotFound)
		MetricListErrors.Inc()
		MetricNotFound.Inc()
		return
	}
	MetricListSuccess.Inc()
	httputils.ReturnJSON(w, http.StatusOK, versions)
}

// PackageMeta returns download URLs and associated metadata for the distribution
// packages for a particular version of a provider.
// https://www.terraform.io/docs/internals/provider-network-mirror-protocol.html#list-available-installation-packages
func (svc *MirrorService) PackageMeta(w http.ResponseWriter, r *http.Request) {
	id := tracer.GetRequestId(r)
	vars := mux.Vars(r)
	hostname := vars["hostname"]
	namespace := vars["namespace"]
	name := vars["type"]
	vers := vars["version"]

	provider := &addrs.Provider{
		Type:      name,
		Namespace: namespace,
		Hostname:  svchost.Hostname(hostname),
	}
	log.WithField("id", id).Infof("loaded provider: %s", provider.ForDisplay())

	log.WithField("id", id).Debugf("attempting to parse version '%s'", vers)
	v, err := version.NewVersion(vers)
	if err != nil {
		log.WithError(err).Error("failed to parse version")
		http.Error(w, "failed to parse version", http.StatusBadRequest)
		MetricGetErrors.Inc()
		return
	}

	packages, err := svc.registry.PackageMeta(r.Context(), provider, *v)
	if err != nil {
		log.WithError(err).WithField("id", id).Error("failed to retrieve package data")
		http.Error(w, "failed to retrieve package data", http.StatusBadRequest)
		MetricGetErrors.Inc()
		return
	}
	if len(packages.Archives) == 0 {
		log.WithField("id", id).Info("found 0 versions, lets assume that means this provider doesn't exist")
		http.Error(w, "404 not found", http.StatusNotFound)
		MetricGetErrors.Inc()
		MetricNotFound.Inc()
		return
	}
	MetricGetSuccess.Inc()
	httputils.ReturnJSON(w, http.StatusOK, packages)
}
