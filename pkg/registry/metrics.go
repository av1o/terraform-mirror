package registry

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	MetricS3Signs = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_registry_s3_sign_total",
		Help: "Total number of S3 signatures",
	})
	MetricS3SignErr = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_registry_s3_sign_errs_total",
		Help: "Total number of S3 signature errors",
	})
	MetricS3Sessions = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_registry_s3_session_total",
		Help: "Total number of S3 sessions",
	})
	MetricS3SessionErr = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_registry_s3_session_errs_total",
		Help: "Total number of S3 session errors",
	})
	MetricS3Lists = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_registry_s3_list_total",
		Help: "Total number of S3 lists",
	})
	MetricS3ListErr = promauto.NewCounter(prometheus.CounterOpts{
		Name: "tfm_registry_s3_list_errs_total",
		Help: "Total number of S3 list errors",
	})
)
