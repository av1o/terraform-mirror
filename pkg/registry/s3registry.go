package registry

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/djcass44/go-tracer/tracer"
	"github.com/hashicorp/go-version"
	"github.com/hashicorp/terraform/addrs"
	log "github.com/sirupsen/logrus"
	"gitlab.dcas.dev/open-source/tf-registry/pkg/utils"
	"strings"
	"time"
)

type S3Config struct {
	Bucket    string
	Region    string
	Endpoint  string
	PathStyle bool
}

type S3Registry struct {
	conf *S3Config
}

func NewS3Registry(conf *S3Config) *S3Registry {
	r := new(S3Registry)
	r.conf = conf

	return r
}

func (r *S3Registry) AvailableVersions(ctx context.Context, provider *addrs.Provider) (*AvailableVersionsResponse, error) {
	id := tracer.GetContextId(ctx)

	key := fmt.Sprintf("%s/%s/%s", provider.Hostname, provider.Namespace, provider.Type)
	log.WithField("id", id).Infof("loading available versions for: %s", key)

	objects, err := r.listFiles(ctx, key)
	if err != nil {
		log.WithError(err).WithField("id", id).Error("failed to list s3 files")
		return nil, err
	}
	var versions = map[string]struct{}{}
	for _, o := range objects.Contents {
		parts := strings.Split(*o.Key, "/")
		log.WithField("id", id).Debugf("located object: %s, %v", *o.Key, parts)
		versions[parts[3]] = struct{}{}
	}

	return &AvailableVersionsResponse{Versions: versions}, nil
}

func (r *S3Registry) PackageMeta(ctx context.Context, provider *addrs.Provider, version version.Version) (*PackageMetaResponse, error) {
	id := tracer.GetContextId(ctx)

	key := fmt.Sprintf("%s/%s/%s/%s", provider.Hostname, provider.Namespace, provider.Type, version.String())
	log.WithField("id", id).Infof("loading available packages for: %s@%s", key, version.String())

	objects, err := r.listFiles(ctx, key)
	if err != nil {
		log.WithError(err).Error("failed to list s3 files")
		return nil, err
	}
	var archives = map[string]Archive{}
	for _, o := range objects.Contents {
		parts := strings.Split(*o.Key, "/")
		log.WithField("id", id).Debugf("located object: %s, %v", *o.Key, parts)
		name, err := utils.ParseProviderPackage(parts[4])
		if err != nil {
			log.WithError(err).WithField("id", id).Errorf("failed to parse object: '%s'", parts[4])
			continue
		}
		url, err := r.getPresigned(ctx, *o.Key)
		if err != nil {
			log.WithError(err).WithField("id", id).Errorf("failed to pre-sign key: %s", *o.Key)
			continue
		}
		archives[fmt.Sprintf("%s_%s", name.OS, name.Arch)] = Archive{
			URL:    url,
			Hashes: []string{},
		}
	}

	return &PackageMetaResponse{Archives: archives}, nil
}

// getPresigned creates an S3 presigned download link
func (r *S3Registry) getPresigned(ctx context.Context, key string) (string, error) {
	id := tracer.GetContextId(ctx)
	log.WithField("id", id).Infof("generating pre-signed url for key: %s", key)
	s3client := s3.New(r.getSession())
	req, _ := s3client.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(r.conf.Bucket),
		Key:    aws.String(key),
	})
	// sign for 1 minute, which should be plenty for Terraform to download
	signedURL, err := req.Presign(1 * time.Minute)
	if err != nil {
		log.WithError(err).WithField("id", id).Errorf("failed to pre-sign key: %s", key)
		MetricS3SignErr.Inc()
		return "", err
	}
	MetricS3Signs.Inc()
	return signedURL, nil
}

func (r *S3Registry) listFiles(ctx context.Context, prefix string) (*s3.ListObjectsOutput, error) {
	id := tracer.GetContextId(ctx)

	s3client := s3.New(r.getSession())
	loi := &s3.ListObjectsInput{
		Bucket: aws.String(r.conf.Bucket),
		Prefix: aws.String(prefix),
	}
	log.Infof("listing objects in bucket %s, with prefix %s", r.conf.Bucket, *loi.Prefix)
	result, err := s3client.ListObjects(loi)
	if err != nil {
		MetricS3ListErr.Inc()
		log.WithError(err.(awserr.Error)).WithField("id", id).Error("failed to list s3 objects")
		return nil, err
	}
	MetricS3Lists.Inc()
	log.WithField("id", id).Debugf("successfully listed bucket with %d result(s)", len(result.Contents))
	return result, nil
}

// getSession creates a new S3 session
func (r *S3Registry) getSession() *session.Session {
	config := &aws.Config{
		S3ForcePathStyle:              aws.Bool(r.conf.PathStyle),
		Region:                        aws.String(r.conf.Region),
		CredentialsChainVerboseErrors: aws.Bool(true),
	}
	// special config if we're using a non-aws s3
	if r.conf.Endpoint != "" {
		if !strings.HasPrefix(r.conf.Endpoint, "https") {
			log.Warning("disabling TLS for s3 endpoint - this is not recommended")
			config.DisableSSL = aws.Bool(true)
		}
		config.Endpoint = aws.String(r.conf.Endpoint)
	}
	s, err := session.NewSession(config)
	if err != nil {
		MetricS3SessionErr.Inc()
		log.WithError(err).Error("failed to create S3 session")
		panic(err) // we need to fail out hard on auth issues
	}
	MetricS3Sessions.Inc()
	return s
}
