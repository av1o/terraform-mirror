package registry

import (
	"context"
	"github.com/hashicorp/go-version"
	"github.com/hashicorp/terraform/addrs"
)

type AvailableVersionsResponse struct {
	Versions map[string]struct{} `json:"versions"`
}

// https://www.terraform.io/docs/internals/provider-network-mirror-protocol.html#sample-response-1
type PackageMetaResponse struct {
	Archives map[string]Archive `json:"archives"`
}

type Archive struct {
	URL    string   `json:"url"`
	Hashes []string `json:"hashes"`
}

// ProviderRegistry determines the minimum required to implement the "Provider Network Mirror Protocol"
type ProviderRegistry interface {
	AvailableVersions(ctx context.Context, provider *addrs.Provider) (*AvailableVersionsResponse, error)
	PackageMeta(ctx context.Context, provider *addrs.Provider, version version.Version) (*PackageMetaResponse, error)
}
